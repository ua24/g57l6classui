//
//  Weapon.swift
//  G57L6
//
//  Created by Ivan Vasilevich on 10/12/17.
//  Copyright © 2017 Smoosh Labs. All rights reserved.
//

import UIKit

class Weapon: NSObject {
	
	enum Material: Int {
		case wood = 1
		case titan
		case iron
		case woodIron
	}
	
	var autoReload = false
	private var ammoCount = 0
//	var consistOf: Material = .iron
	var consistOf = Material.iron
	let oboyma: Int
	static var totalWeaponsCount = 0
	
	override var description: String {
		return """
		Material:\t\(consistOf)
		ammoCount:\t\(ammoCount)
		oboyma:\t\t\(oboyma)
		autoReload:\t\(autoReload)
		nsobjectDescript:\t\(super.description)
		"""
	}
	
	init(oboymaCount: Int) {
		self.oboyma = oboymaCount
		Weapon.totalWeaponsCount += 1
	}
	
	func reload() {
		print("chik chik")
		ammoCount = oboyma
		shoot()
	}
	//incapsulation private
	func shoot() {
		if ammoCount > 0 {
			print("pew pew")
			ammoCount -= 1
		}
		else {
			print("click")
			if autoReload {
				reload()
			}
			
		}
	}
	


}
